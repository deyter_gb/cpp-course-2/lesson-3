#include <iostream>
#include <cmath>
#include <utility>

const std::string TASK_SEPARATOR = "========================================================";
/**
 * Print separator between tasks.
 */
void PrintSeparator() {
  std::cout << TASK_SEPARATOR << std::endl;
}

class Figure {
 public:
  [[nodiscard]] virtual double area() const = 0;
};

class Parallelogram : public Figure {
 protected:
  double height;
  double base;
 public:
  Parallelogram(double height, double base): height(height), base(base) {};
  [[nodiscard]] double area() const override {
    return height * base;
  }
};

class Circle : public Figure {
 protected:
  double radius;
 public:
  explicit Circle(double radius): radius(radius) {};
  [[nodiscard]] double area() const override {
    return radius * radius * M_PI;
  }
};

class Rectangle : public Parallelogram {
 public:
  Rectangle(double height, double base): Parallelogram(height, base) {};
};

class Square : public Parallelogram {
 public:
  explicit Square(double base): Parallelogram(base, base) {};
};

class Rhombus : public Parallelogram {
 public:
  explicit Rhombus(double base): Parallelogram(base, base) {};
};


class Car {
 protected:
  std::string company;
  std::string model;
 public:
  Car() = default;
  Car(std::string company, std::string model): company(std::move(company)), model(std::move(model)) {
    std::cout << std::endl << "Car constructor" << std::endl;
  }
  ~Car() = default;
  [[nodiscard]] std::string getCompany() const {
    return company;
  }
  [[nodiscard]] std::string getModel() const {
    return model;
  }
};

class PassengerCar : virtual public Car {
 public:
  PassengerCar() = default;
  PassengerCar(std::string company, std::string model): Car(std::move(company), std::move(model)) {
    std::cout << "PassengerCar constructor" << std::endl;
  }
};

class Bus : virtual public Car {
 public:
  Bus() = default;
  Bus(std::string company, std::string model): Car(std::move(company), std::move(model)) {
    std::cout << "Bus constructor" << std::endl;
  }
};

class Minivan : public Bus, public PassengerCar{
 public:
  Minivan(std::string company, std::string model): Car(std::move(company), std::move(model)) {
    std::cout << "Minivan constructor" << std::endl;
  }
};

class Fraction {
 protected:
  float numerator;
  float denominator;
 public:
  Fraction(float numerator, float denominator): numerator(numerator), denominator(denominator) {};
  void print() const {
    std::cout << numerator << "/" << denominator << std::endl;
  }
  [[nodiscard]] float getNumerator() const {
    return numerator;
  }
  [[nodiscard]] float getDenominator() const {
    return denominator;
  }
  Fraction* operator+(Fraction& f) const {
    return new Fraction(this->numerator * f.denominator + f.numerator * this->denominator, this->denominator * f.denominator);
  }
  Fraction* operator-(Fraction& f) const {
    return *this + *(- f);
  }
  Fraction* operator-() const {
    return new Fraction(-numerator, denominator);
  }
  Fraction* operator*(Fraction& f) const {
    return new Fraction(this->numerator * f.numerator, this->denominator * f.denominator);
  }
  Fraction* operator/(Fraction& f) const {
    return new Fraction(this->numerator * f.denominator, this->denominator * f.numerator);
  }
  bool operator==(Fraction& f) const {
    return (this->numerator * f.denominator == f.numerator * this->denominator);
  }
  bool operator!=(Fraction& f) const {
    return !(*this == f);
  }
  bool operator>(Fraction& f) const {
    return (this->numerator * f.denominator > f.numerator * this->denominator);
  }
  bool operator<(Fraction& f) const {
    return !(*this > f) && (*this != f);
  }
  bool operator>=(Fraction& f) const {
    return (*this > f) || (*this == f);
  }
  bool operator<=(Fraction& f) const {
    return (*this < f) || (*this == f);
  }
};

void printCar(Car &car) {
  std::cout << "Car company:" << car.getCompany() << ", model:" << car.getModel() << std::endl;
}

void printArea(Figure& figure) {
  std::cout << "Area of figure is " << figure.area() << std::endl;
}

int main() {
  /**
   * Task 1
   * Создать абстрактный класс Figure (фигура).
   * Его наследниками являются классы Parallelogram (параллелограмм) и Circle (круг).
   * Класс Parallelogram — базовый для классов Rectangle (прямоугольник), Square (квадрат), Rhombus (ромб).
   * Для всех классов создать конструкторы. Для класса Figure добавить чисто виртуальную функцию area() (площадь).
   * Во всех остальных классах переопределить эту функцию, исходя из геометрических формул нахождения площади.
   */
  {
    auto prlgrm = new Parallelogram(2, 5);
    printArea(*prlgrm);
    delete prlgrm;
    auto circle = new Circle(2);
    printArea(*circle);
    delete circle;
    auto rctngl = new Rectangle(2, 3);
    printArea(*rctngl);
    delete rctngl;
    auto square = new Square(2);
    printArea(*square);
    delete square;
    auto rhombus = new Rhombus(3);
    printArea(*rhombus);
    delete rhombus;
  }
  PrintSeparator();

  /**
   * Task 2
   * Создать класс Car (автомобиль) с полями company (компания) и model (модель).
   * Классы-наследники: PassengerCar (легковой автомобиль) и Bus (автобус).
   * От этих классов наследует класс Minivan (минивэн).
   * Создать конструкторы для каждого из классов, чтобы они выводили данные о классах.
   * Создать объекты для каждого из классов и посмотреть, в какой последовательности выполняются конструкторы.
   * Обратить внимание на проблему «алмаз смерти».
   */
  {
    auto car = new Car("Lexus", "LX500d");
    printCar(*car);
    delete car;
    auto bus = new Bus("Mercedes-Benz", "OM 926 LA");
    printCar(*bus);
    delete bus;
    auto p_car = new PassengerCar("Volvo", "C40");
    printCar(*p_car);
    delete p_car;
    auto minivan = new Minivan("JAC", "Sunray");
    printCar(*minivan);
    delete minivan;
  }
  PrintSeparator();

  /**
   * Task 3
   * Создать класс: Fraction (дробь). Дробь имеет числитель и знаменатель (например, 3/7 или 9/2).
   * Предусмотреть, чтобы знаменатель не был равен 0. Перегрузить:
   * математические бинарные операторы (+, -, *, /) для выполнения действий с дробями
   * унарный оператор (-)
   * логические операторы сравнения двух дробей (==, !=, <, >, <=, >=).
   */
  {
    Fraction d1(1, 7);
    Fraction d2(3, 11);
    (d1 + d2)->print();
    (d1 - d2)->print();
    (d1 * d2)->print();
    (d1 / d2)->print();
    (-d1)->print();
    if (d1 == d2) std::cout << "\nFraction 1 == Fraction 2";
    if (d1 != d2) std::cout << "\nFraction 1 != Fraction 2";
    if (d1 > d2) std::cout << "\nFraction 1 > Fraction 2";
    if (d1 <= d2) std::cout << "\nFraction 1 <= Fraction 2";
    if (d1 < d2) std::cout << "\nFraction 1 < Fraction 2";
    if (d1 >= d2) std::cout << "\nFraction 1 >= Fraction 2";
  }

  return 0;
}
